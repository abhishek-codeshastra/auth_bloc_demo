import 'dart:developer';

import 'package:bloc/bloc.dart';

/// [BlocObserver] for the application which
/// observes all state changes.
class MyBlocObserver extends BlocObserver {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    log("$transition", name: " 🔷 ${bloc.runtimeType} 🔷 ");
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    log(
      "Error in ${bloc.runtimeType} !!",
      name: "${bloc.runtimeType}",
      error: error,
      stackTrace: stackTrace,
    );
    super.onError(bloc, error, stackTrace);
  }
}

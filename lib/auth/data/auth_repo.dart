import 'dart:async';

import '../bloc/auth_bloc.dart';
import 'auth_api_service.dart';

class AuthRepo {
  final AuthApiService _authApiService;
  final _controller = StreamController<AuthState>();

  AuthRepo(this._authApiService);

  //? stream of auth status
  Stream<AuthState> get status async* {
    // await splash.future;
    yield* _controller.stream;
  }

  //? on app launch check credentials
  void checkAndUpdateAuthStatus() async {
    // final user = AppUserStorageHelper.read()?.user;
    // if (user != null || (user.token.expires.isBefore(DateTime.now()))) {
    //   StorageHelper.instance.clear();
    //   _controller.add(AuthStatus.unauthenticated);
    // } else {
    //   AppUserRepo.refreshToken();
    //   _controller.add(AuthStatus.authenticated);
    // }
    _controller.add(AuthState.unauthenticated);
  }

  //? login
  Future<void> login({
    required String mobile,
  }) async {
    final loginData = await _authApiService.login();
    // StorageHelper.instance.add(loginData);
    _controller.add(AuthState.authenticated);
  }

  //? logout
  void logOut() {
    // StorageHelper.instance.clear();
    _controller.add(AuthState.unauthenticated);
  }

  void dispose() => _controller.close();
}

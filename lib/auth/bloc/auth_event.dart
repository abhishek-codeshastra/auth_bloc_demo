part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class AuthStatusChanged extends AuthEvent {
  AuthStatusChanged(this.status);
  final AuthState status;

  @override
  String toString() => 'AuthStatusChanged(status: $status)';
}

class AuthLogoutRequested extends AuthEvent {
  @override
  String toString() => 'AuthLogoutRequested()';
}

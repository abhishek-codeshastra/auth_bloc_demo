part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginSubmittedEvent extends LoginEvent {
  final String mobile;

  LoginSubmittedEvent({required this.mobile});
}

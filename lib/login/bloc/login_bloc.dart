import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../auth/data/auth_repo.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepo authRepo;
  LoginBloc(this.authRepo) : super(LoginInitialState()) {
    on<LoginSubmittedEvent>(_onLoginSubmittedEvent);
  }

  Future<void> _onLoginSubmittedEvent(
    LoginSubmittedEvent event,
    Emitter<LoginState> emit,
  ) async {
    emit(LoginInProgressState());
    await Future.delayed(const Duration(seconds: 2));

    try {
      await authRepo.login(mobile: event.mobile);
      emit(LoginSuccess());
    } on DioError catch (e, st) {
      log("Error in login", error: e, stackTrace: st);
      emit(LoginFailedState("errorMessage"));
    } catch (e, st) {
      log("Error in login", error: e, stackTrace: st);
      emit(LoginFailedState("errorMessage"));
    }
  }
}

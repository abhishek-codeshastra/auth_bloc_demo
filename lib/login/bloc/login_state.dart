part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitialState extends LoginState {}

class LoginInProgressState extends LoginState {}

class LoginFailedState extends LoginState {
  final String errorMessage;

  LoginFailedState(this.errorMessage);
}

class LoginSuccess extends LoginState {}

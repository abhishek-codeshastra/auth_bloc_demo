import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/login_bloc.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);

  final formKey = GlobalKey<FormState>();

  void _handleOnPress(BuildContext context) {
    if (formKey.currentState!.validate()) {
      context.read<LoginBloc>().add(LoginSubmittedEvent(mobile: ""));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailedState) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Text(state.errorMessage),
              ),
            );
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: Center(
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                //? mobile
                TextFormField(
                  validator: (value) {
                    if (value == null || value == "") {
                      return "Enter Mobile Number";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    label: Text("Mobile"),
                  ),
                ),

                //? login button
                BlocBuilder<LoginBloc, LoginState>(
                  builder: (context, state) {
                    if (state is LoginInProgressState) {
                      return const CircularProgressIndicator();
                    }
                    return TextButton(
                      onPressed: () => _handleOnPress(context),
                      child: const Text("Login"),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'auth/bloc/auth_bloc.dart';
import 'auth/data/auth_api_service.dart';
import 'auth/data/auth_repo.dart';
import 'home/home_page.dart';
import 'login/bloc/login_bloc.dart';
import 'login/view/login_page.dart';
import 'splash/splash_page.dart';
import 'utils/my_bloc_observer.dart';

void main() {
  BlocOverrides.runZoned(
    () => runApp(MyApp()),
    blocObserver: MyBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  //? navigator
  final _navigatorKey = GlobalKey<NavigatorState>();
  NavigatorState get _navigator => _navigatorKey.currentState!;

  //? create auth repo and call initial method
  final authRepo = AuthRepo(AuthApiService())..checkAndUpdateAuthStatus();

  @override
  Widget build(BuildContext context) {
    //? Material app with all the blocs
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBloc(authRepo),
        ),
        BlocProvider(
          create: (context) => LoginBloc(authRepo),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        navigatorKey: _navigatorKey,
        builder: (context, widget) {
          return BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              switch (state) {
                case AuthState.unknown:
                  _navigator.pushAndRemoveUntil<void>(
                    MaterialPageRoute(builder: (context) => const SplashPage()),
                    (route) => false,
                  );
                  break;
                case AuthState.authenticated:
                  _navigator.pushAndRemoveUntil<void>(
                    MaterialPageRoute(builder: (context) => const HomePage()),
                    (route) => false,
                  );
                  break;
                case AuthState.unauthenticated:
                  _navigator.pushAndRemoveUntil<void>(
                    MaterialPageRoute(builder: (context) => LoginPage()),
                    (route) => false,
                  );
                  break;
              }
            },
            child: widget!,
          );
        },
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: const SplashPage(),
      ),
    );
  }
}
